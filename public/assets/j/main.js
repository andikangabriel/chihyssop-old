$(document).ready(function(){
  $('.menuicon').click(function() {
    $(this).toggleClass("collapse");
    $('header').toggleClass("collapse");
    $('nav').toggleClass("collapse");
  });

  $('.slider').slick({
      fade: true,
      dots: true,
      infinite: false,
      speed: 500,
      cssEase: 'linear',
      draggable: true,
      swipe: true,
      touchMove: true,
      autoplay: true,
      prevArrow: false,
      nextArrow: true
  });

  var $header = $("header"),
      $clone = $header.before($header.clone().addClass("clone"));

  $(window).on("scroll", function() {
    var fromTop = $(window).scrollTop();
    $('body').toggleClass("down", (fromTop > 200));
  });
})
