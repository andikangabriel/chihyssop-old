module.exports = {
  theme: {
    extend: {
      screens: {
        400: '400px'
      },
      fontFamily: {
        display: ['PT Sans Narrow', 'sans-serif'],
        body: ['PT Sans', 'sans-serif'],
        opensans: ['Open Sans', 'sans-serif']
      },
      colors: {
        transparent: 'transparent',

        black: '#000',
        white: '#fff',

        accent: {
          dark: 'hsl(300, 100%, 20%)',
          light: 'hsl(300, 100%, 30%)',
        },

        link: 'hsl(330, 100%, 20%)',
        hover: 'hsl(150, 50%, 30%)',

        support: {
          info: 'hsl(203, 85%, 54%)',
          error: 'hsl(349, 100%, 35%)',
          success: 'hsl(116, 46%, 49%)',
          warning: 'hsl(34, 100%, 63%)',
        },

        brand: {
          'dark': 'hsl(150, 100%, 20%)',
          'light': 'hsl(153, 100%, 39%)',
        },
        secondary: {
          'base': 'hsl(240, 33%, 30%)',
          'light-1': 'hsl(240, 18%, 44%)',
          'light-2': 'hsl(240, 14%, 58%)',
          'light-3': 'hsl(240, 33%, 72%)',
          'light-4': 'hsl(240, 18%, 86%)',
          'light-5': 'hsl(240, 14%, 93%)',
          'light-6': 'hsl(240, 33%, 50%)',
          'dark-1': 'hsl(240, 33%, 24%)',
          'dark-2': 'hsl(240, 33%, 18%)',
          'dark-3': 'hsl(240, 34%, 12%)',
          'dark-4': 'hsl(240, 33%, 6%)',
          'dark-5': 'hsl(240, 33%, 3%)',
        },
        read: {
          100: 'hsl(227, 20%, 17%)',
          200: 'hsl(225, 12%, 25%)',
          300: 'hsl(228, 8%, 34%)',
          400: 'hsl(229, 4%, 50%)',
          500: 'hsl(223, 4%, 67%)',
          600: 'hsl(225, 5%, 84%)',
          700: 'hsl(240, 5%, 92%)',
          "inverse": 'hsl(0, 0%, 100%)',
        },
        ui: {
          100: 'hsl(0, 0%, 100%)',
          200: 'hsl(240, 33%, 99%)',
          300: 'hsl(220, 11%, 95%)',
          400: 'hsl(228, 6%, 84%)',
          500: 'hsl(227, 10%, 36%)',
          600: 'hsl(227, 20%, 17%)',
          700: 'hsl(0, 0%, 0%)',
        },
        gray: {
          100: '#f7fafc',
          200: '#edf2f7',
          300: '#e2e8f0',
          400: '#cbd5e0',
          500: '#a0aec0',
          600: '#718096',
          700: '#4a5568',
          800: '#2d3748',
          900: '#1a202c',
        },
      },
    }
  },
  variants: {},
  plugins: []
}
