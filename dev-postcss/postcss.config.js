// postcss configurations
module.exports = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
    require('@fullhuman/postcss-purgecss')({
      content: ['./public/*.html', './build/*.html'],
      extensions: ["html", "js", "php", "vue"],
      whitelistPatterns: [/collapse/, /slick/, /down/, /clone/],
      defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
    })
  ]
}
